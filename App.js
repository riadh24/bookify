
import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import MyStack from './src/navigation/navigation'
import { Provider } from 'react-redux';
import Store  from './src/redux/store'
import { PersistGate } from 'redux-persist/es/integration/react'
import { ApplicationProvider,IconRegistry } from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import * as eva from '@eva-design/eva';

const App = () => {
  return (
   <View style={styles.container}>
    <Provider store={Store.store}>
    <PersistGate loading={null} persistor={Store.persistor} >
    <ApplicationProvider {...eva} theme={eva.light}>
      <IconRegistry icons={EvaIconsPack} />
      <MyStack/>
    </ApplicationProvider>
    </PersistGate>
    </Provider>
   </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

   
  
});

export default App;

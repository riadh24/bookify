import { createStore,applyMiddleware ,compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './combineReducer'
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage';
import { createLogger } from 'redux-logger'

const persistConfig = {
  timeout: 10000,
    key: 'root',
    storage: AsyncStorage,
    whitelist : ['authReducer']
  }
  const pesistedReducer = persistReducer(persistConfig,rootReducer)

let composeEnhancers = compose;
  

if(__DEV__){
composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}
const store = createStore(pesistedReducer,composeEnhancers(applyMiddleware(thunk,createLogger())));
let persistor = persistStore(store);
export default {store,persistor};
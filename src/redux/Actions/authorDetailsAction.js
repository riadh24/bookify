import { DETAILS_AUTHOR, LIST_BOOKS_AUTHOR } from "./type";
import { httpClient } from "../../navigation/app_interceptor";

function DetailAuthor(data) {
  return {
    type: DETAILS_AUTHOR,
    payload: data,
  };
}

function setlistBooks(data) {
  return {
    type: LIST_BOOKS_AUTHOR,
    payload: data,
  };
}

export const authordetail = (params) => {
  return (dispatch) => {
    httpClient.get(`/authors/${params}`).then((res) => {
      dispatch(DetailAuthor(res.data));
    });
    httpClient.post("/author_list_books", { author_id: params }).then((res) => {
      dispatch(setlistBooks(res.data))
    });
  };
};

import {
  BOOKS_SUCCESS,
  BOOKS_FAILURE,
  DETAIL_SUCCESS,
  DETAIL_FAILURE,
  FAVORITES_FAILURE,
  FAVORITES_SUCCESS,
  ADD_FAILURE,
  ADD_SUCCESS,
  DELETE_FAVORITE_FAILURE,
  DELETE_FAVORITE_SUCCESS,
} from "./booksType";
import { httpClient } from "../../navigation/app_interceptor";

function DeletFavoriteFailure(err) {
  return {
    type: DELETE_FAVORITE_FAILURE,
    err: err,
  };
}

function AddFailure(err) {
  return {
    type: ADD_FAILURE,
    err: err,
  };
}

function FavoritesFailure(err) {
  return {
    type: FAVORITES_FAILURE,
    err: err,
  };
}
function BooksFailure(err) {
  return {
    type: BOOKS_FAILURE,
    err: err,
  };
}

function detailFailure() {
  return {
    type: DETAIL_FAILURE,
  };
}

export const ListeBooks = () => {
  return (dispatch) => {
    httpClient
      .get("/book")
      .then((responseJson) => {
        console.log(responseJson);
        dispatch({
          type: BOOKS_SUCCESS,
          payload: responseJson.data,
        });
      })
      .catch((err) => dispatch(BooksFailure(err)));
  };
};
export const detailBooks = (id) => {
  return (dispatch) => {
    httpClient
      .get(`/book/${id}`)
      .then((responseJson) => {
        console.log(responseJson);
        dispatch({
          type: DETAIL_SUCCESS,
          payload: responseJson.data,
        });
      })
      .catch((err) => dispatch(detailFailure(err)));
  };
};
export const ListeFavorites = () => {
  return (dispatch) => {
    httpClient
      .get("/favorites")
      .then((responseJson) => {
        console.log(responseJson);
        dispatch({
          type: FAVORITES_SUCCESS,
          payload: responseJson.data,
        });
      })
      .catch((err) => dispatch(FavoritesFailure(err)));
  };
};
export const updateFavorite = (userId, bookId) => {
  return (dispatch) => {
    httpClient
      .post(
        "/add_or_update_favorite",
        {
          "book_id": bookId,
          "user_id": userId,
          "favorite_book": favorite_book,
        },
        {
          headers: {},
        }
      )
      .then((responseJson) => {
        console.log(responseJson);
        dispatch({
          type: ADD_SUCCESS,
          payload: responseJson.data,
        });
      })
      .catch((err) => dispatch(AddFailure(err)));
  };
};

export const DeleteFavorites = (userId, bookId,favorite_book) => {
 
  return (dispatch) => {
    httpClient
      .post("/add_or_update_favorite", {
        "book_id": bookId,
        "user_id": userId,
        "favorite_book": favorite_book,
        
      })
      .then((responseJson) => {
        console.log(
          'im user ID -------------',responseJson.data
        );
        dispatch({
          type: DELETE_FAVORITE_SUCCESS,
          payload: responseJson.data,
        });
      })
      .catch((err) => dispatch(DeletFavoriteFailure(err)));
  };
};

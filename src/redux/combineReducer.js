import { combineReducers } from "redux";
import {booksReducer} from './Reducers/booksReducer';
import {authReducer} from './Reducers/AuthReducer';
import {authorReducer} from './Reducers/authorDetailsReducer';

const rootReducer = combineReducers({
    booksReducer:booksReducer,
    authReducer:authReducer,
    authorReducer
})
export default rootReducer ;
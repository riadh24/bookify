import {AUTH_SUCCESS} from '../Actions/AuthTypes';

const initialState = {
    token:"",
}
export  function authReducer(state = initialState, action) {
    switch(action.type) {
        case  AUTH_SUCCESS :
            return {
                ...state,
                token: action.payload,
            }
        default:
            return state
    }
}
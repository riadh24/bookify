import {BOOKS_SUCCESS,
    DETAIL_SUCCESS,
    FAVORITES_SUCCESS,
    ADD_SUCCESS,
    DELETE_FAVORITE_SUCCESS} from '../Actions/booksType';

const initialState = {
    books:{img:""},
    detail:{book:{img:""},author:{img:""}},
    favorites:{img:""},
    favoriteslist:{img:""},
}
export  function booksReducer(state = initialState, action) {
 console.log("action",action.payload);
    switch(action.type) {
        case  BOOKS_SUCCESS :
            return {
                ...state,
                books: action.payload,
            }
            case  DETAIL_SUCCESS :
                return {
                    ...state,
                    detail: action.payload,
                }
                case  FAVORITES_SUCCESS :
                    return {
                        ...state,
                        favoriteslist: action.payload,
                    }
                    case  ADD_SUCCESS :
                    return {
                        ...state,
                        favorites: action.payload,
                    }
                    case  DELETE_FAVORITE_SUCCESS :
                        return {
                            ...state,
                            favorites: action.payload,
                        }
        default:
            return state
    }
}
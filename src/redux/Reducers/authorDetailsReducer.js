import { DETAILS_AUTHOR,LIST_BOOKS_AUTHOR } from "../Actions/type";
const initialState = {
    author_details: {img:""},
    listBooks: []
};
export function authorReducer(state = initialState, action) {
  switch (action.type) {
    case DETAILS_AUTHOR:
      return {
        ...state,
        author_details: action.payload,
      };
      case LIST_BOOKS_AUTHOR:
        return {
          ...state,
          listBooks: action.payload,
        };
    default:
      return state;
  }
}

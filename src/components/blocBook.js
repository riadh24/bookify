
import React from 'react';
import { Dimensions, View, StyleSheet, Text,TouchableOpacity,Image} from 'react-native';
const deviceHeight = Dimensions.get('window').height;
 const deviceWidth = Dimensions.get('window').width;
 
export const BlocBook = ({ item , GetDetail}) => (
    <TouchableOpacity onPress={GetDetail}>
        <View
          style={styles.block}>
          <View style={{alignItems:"center"}}>
            <View style={styles.rectangle}>
                <Image
              style={styles.img}
              source={{uri: item.img}}
            />
            </View>
            <View style={{flexDirection:"row"}}>
            <Text
              style={styles.title}>
              {item.title}
            </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
  );
  const styles = StyleSheet.create({
    rectangle: {
      width: 150,
      height: 200,
      top: deviceHeight * 0.015,
      alignItems:"center"
    },
    img: {
      width: 150,
      height: 180,
    },
    block:{
      width:165,
      backgroundColor: '#454545',
      marginHorizontal: deviceWidth * 0.03,
      top: 25,
      marginBottom: 25,
      shadowColor: '#666',
      borderRadius: 20,
      alignItems:"center"
    },
    title:{
      width: deviceWidth * 0.7,
      fontFamily: 'Raleway-Bold',
      fontSize: deviceHeight * 0.02,
      lineHeight: 20,
      color: '#FFF',
      textAlign:'center',
      justifyContent: 'center',
      marginBottom:10,
      flexShrink: 1  
    },

  });

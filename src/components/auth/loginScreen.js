import React from 'react';
import {View,ScrollView, TouchableWithoutFeedback,Image,ImageBackground,StyleSheet} from 'react-native';
import {
  Button,
  Icon,
  Input,
  Layout,
  Text
} from '@ui-kitten/components';
import { useDispatch, useSelector } from "react-redux";
import axios from 'axios';
import { httpClient } from "../../navigation/app_interceptor";
import AsyncStorage from '@react-native-community/async-storage';
export default LoginScreen = ({navigation}) => {
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);
  const [error, setError] = React.useState(null);
  const [form, setForm] = React.useState({
    email: '',
    password: ''
  });
 
  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };
  const renderIcon = (props) => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'} />
    </TouchableWithoutFeedback>
  );
 

 const storeData = async (value) => {
    try {
      const jsonValue = JSON.stringify(value);
      await AsyncStorage.setItem('@data', jsonValue);
    } catch (e) {
      // saving error
    }
  } 

  const submit = () => {
   console.log('submit');
  
    httpClient.post('/login',form)
    .then(
        (result) => {
          console.log(
            'login resss',result.data
          );
          if (result.data.token) {
            setError(false);
            console.log('result.data.token',result.data.token);
            storeData(result.data).then(() => {
              navigation.navigate('MyTabs');
            });
          } else {
            //error here
            setError(true);
          }
        },
        (error) => {
          console.log('my result ', error);
          setError(true);
        },
      ).catch(e=>console.log('error',e))
  };
  return (
    <ImageBackground source={require('../../assets/background2.jpg')} style={styles.image}>
    
      <ScrollView style={{flex: 1}}>
        <View style={{flex: 1, margin: 40}}>
        <View style={{height: 130, alignItems: 'center'}}>
            <Image
              style={{width: 120, height: 120}}
              source={require('../../assets/bookLogo.png')}
            />
          </View>
          <View style={{flex: 1}}>
            <View style={{alignItems:"center",paddingBottom: 20}}>
              <Text category="h2" style={{color:"white"}}>Bienvenu</Text>
              <Text category="p2" style={{color:"white"}} >
                Veuillez entrer votre login et mot de passe.
              </Text>
              {error && (
                <View style={{paddingTop: 10}}>
                  <Text category="p2" status="danger" appearance="hint">
                    Email ou mot de passe incorrect.
                  </Text>
                </View>
              )}
            </View>
            <Input
              style={{paddingBottom: 20}}
              label="Email"
              placeholder="saisir votre email..."
              onChangeText={(text) => setForm({...form, email: text})}
            />
            <Input
              style={{paddingBottom: 20}}
              label="Mot de passe"
              placeholder="saisir un mot de passe..."
              accessoryRight={renderIcon}
              secureTextEntry={secureTextEntry}
              onChangeText={(text) => setForm({...form, password: text})}
            />
            <Button status="primary" style={{borderRadius:50}} onPress={submit}>
              CONNEXION
            </Button>
          </View>
        </View>
      </ScrollView>
  
    </ImageBackground>
  );
};
const styles = StyleSheet.create({

  image: {
      flex: 1,
      resizeMode: "cover",
      paddingHorizontal: 30,
      paddingTop: 50
  },
  
});

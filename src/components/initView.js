import React, {useEffect} from 'react';
import AsyncStorage from '@react-native-community/async-storage';

export default InitScreen = ({navigation}) => {
  const getUserfromStorage = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem('@data');
      return jsonValue != null ? JSON.parse(jsonValue) : null;
    } catch (e) {
      // error reading value
    }
  };
  useEffect(() => {
    getUserfromStorage().then((result) => {
      console.log('init storage', result);
      if (result) {
        navigation.navigate('MyTabs');
      } else {
        navigation.navigate('LoginScreen');
      }
    });
  });
  return <></>;
};

import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  ScrollView,
  Image,
  TouchableOpacity,
} from "react-native";
import {
  detailBooks,
  updateFavorite,
  DeleteFavorites,
} from "../redux/Actions/booksAction";
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/FontAwesome";
import { Button,Modal } from "@ui-kitten/components";

import Pdf from "react-native-pdf";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const DetailBook = (props) => {
  const [favor, setFavor] = useState("heart-o");
  const [visible, setVisible] = React.useState(false);
  const [currentPage, setCurrentPage] = React.useState(21);
  const DownIcon = (props) => (
    <Icon size={20} color={"rgb(143, 155, 179)"} name="bookmark" />
  );
  const source = {
    // uri: "http://www.africau.edu/images/default/sample.pdf",
   
  };

  const downloadPDF = () => {
    setVisible(!visible);
  };

  useEffect(() => {
    const { id } = props.route.params;
    props.detailbooks(id);
  }, []);

  useEffect(() => {
    const { id } = props.route.params;
    props.detailbooks(id);
    if (props.detailBook?.book?.user_favorite) {
      setFavor("heart");
    } else setFavor("heart-o");
  }, [props?.detailBook?.book?.user_favorite]);

  const handleFavorite = () => {
    if (favor === "heart") {
      setFavor("heart-o");
      props.updateFavorite("1", props.detailBook.book.id,false);
    } else if (favor === "heart-o") {
      setFavor("heart");
      props.updateFavorite("1", props.detailBook.book.id,true);
    }
  };

  return (
    <React.Fragment>
      <View
        style={{
          backgroundColor: "#0A0A0A",
          flex: 1,
        }}
      >
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{ backgroundColor: "#0A0A0A" }}
        >
          <View>
            <View
              style={{
                backgroundColor: "#0A0A0A",
                flex: 1,
              }}
            >
              <ScrollView
                showsVerticalScrollIndicator={false}
                style={{ backgroundColor: "#0A0A0A" }}
              >
                <Image
                  style={styles.rectangle}
                  source={{ uri: props.detailBook.book.img }}
                />
                <View
                  style={{
                    ...styles.collapseble,
                    minHeight: deviceHeight * 0.83,
                  }}
                >
                  <View>
                    <View style={{ flex: 1 }}>
                      <Text
                        style={{
                          color: "#FFF",
                          fontFamily: "Raleway-Bold",
                          fontSize: deviceHeight * 0.03,
                          marginLeft: 10,
                          marginTop: 20,
                          textAlign: "center",
                        }}
                      >
                        {props.detailBook.book.title}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      marginHorizontal: 15,
                      justifyContent: "space-between",
                    }}
                  >
                    <Text
                      style={{
                        color: "#FFF",
                        fontFamily: "Raleway-Bold",
                        fontSize: 16,
                        marginTop: 10,
                      }}
                    >
                      description:
                    </Text>
                    <Text
                      style={{
                        color: "#FFF",
                        opacity: 0.7,
                        fontFamily: "Raleway-Medium",
                        fontSize: 14,
                        marginTop: 10,
                        textAlign: "justify",
                      }}
                    >
                      {props.detailBook.book.description}
                    </Text>
                  </View>
                  <View
                    style={{
                      marginTop: 15,
                      marginHorizontal: 15,
                      justifyContent: "space-between",
                      flexDirection: "row",
                    }}
                  >
                    <Text
                      style={{
                        color: "#FFF",
                        fontFamily: "Raleway-Bold",
                        fontSize: 16,
                        marginTop: 10,
                      }}
                    >
                      auteur:
                    </Text>
                    <TouchableOpacity
                      onPress={() =>
                        props.navigation.navigate("AuthorDetails", {
                          id: props.detailBook.book.author_id,
                        })
                      }
                    >
                      <Text
                        style={{
                          color: "#FFF",
                          opacity: 0.7,
                          fontFamily: "Raleway-Medium",
                          fontSize: 18,
                          marginTop: 10,
                          textAlign: "justify",
                        }}
                      >
                        {props.detailBook?.author?.name}
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View>
                    <Button
                      onPress={() => {
                        downloadPDF();
                      }}
                      style={styles.button}
                      appearance="outline"
                      status="basic"
                      accessoryLeft={DownIcon}
                    >
                      READ ME 
                    </Button>
                  </View>
                  <TouchableOpacity
                    onPress={() => handleFavorite()}
                    style={{
                      alignItems: "flex-end",
                      marginBottom: 10,
                      marginRight: 10,
                    }}
                  >
                    <Icon name={favor} size={50} color="#A020F0" />
                  </TouchableOpacity>
                </View>
              </ScrollView>
            </View>
           
          </View>
        </ScrollView>
     
              <Modal
                visible={visible}
                backdropStyle={styles.backdrop}
                onBackdropPress={() => setVisible(false)}
              >
                <Pdf
                  source={require('../assets/Thrones-Voyager.pdf')}
                  onLoadComplete={(numberOfPages, filePath) => {
                    console.log(`number of pages: ${numberOfPages}`);
                  }}
                  page={currentPage}
                  onPageChanged={(page, numberOfPages) => {
                    console.log(`current page: ${page}`);
                    setCurrentPage(page)
                  }}
                  onError={(error) => {
                    console.log(error);
                  }}
                  onPressLink={(uri) => {
                    console.log(`Link presse: ${uri}`);
                  }}
                  style={styles.pdf}
                  
                />
              </Modal>
            
      </View>
      
    </React.Fragment>
  );
};
const styles = StyleSheet.create({
  container: {
    minHeight: 192,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  pdf: {
    flex: 1,
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
  collapseble: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 20,
    paddingTop: 10,
    shadowOffset: {
      width: 0,
      height: 1,
    },

    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    shadowColor: "#666",
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
    // To round image corners
    overflow: "hidden",
    borderColor: "#999",
    borderWidth: 0,
    backgroundColor: "#0A0A0A",
    // elevation: 10,
    flex: 1,
  },
  button: {
    marginTop: 15,
    width: "50%",
    alignSelf: "flex-end",
  },
  text: {
    color: "rgba(0, 0, 0, 0.5)",
    fontSize: 14,
    fontFamily: "Raleway-Bold",
    marginLeft: 10,
    marginBottom: 25,
    textAlign: "center",
  },
  rectangle: {
    width: "100%",
    height: 180,
    top: deviceHeight * 0.015,
    marginLeft: deviceWidth * 0.02,
    marginRight: deviceWidth * 0.02,
  },
});
function mapStateToProps(state) {
  console.log(state);
  return {
    detailBook: state.booksReducer.detail,
  };
}
const mapDispatchToProps = (dispatch) => ({
  detailbooks: (id) => dispatch(detailBooks(id)),
  updateFavorite: (userId, bookId,favorite_book) => dispatch(updateFavorite(userId, bookId,favorite_book)),
  DeleteFavorite: (userId, bookId,favorite_book) => dispatch(DeleteFavorites(userId, bookId,favorite_book)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailBook);

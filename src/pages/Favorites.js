import React, {  useEffect } from 'react';
import { SafeAreaView, FlatList, StyleSheet,} from 'react-native';
import {BlocBook} from '../components/blocBook.js'
import {ListeFavorites} from "../redux/Actions/booksAction"
import {useDispatch,useSelector} from 'react-redux';

const Favorites = (props) => {
  const renderItem = ({ item }) => (
    <BlocBook item={item} GetDetail={()=>props.navigation.navigate('DetailBook',{id:item.id})} />
  );
    const dispatch = useDispatch()
    const Data = useSelector(state => state.booksReducer.favoriteslist)
  useEffect(() => {
    dispatch(ListeFavorites())
  },[]);

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={Data}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        numColumns={2}
      />
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:"#0A0A0A" ,
alignItems:'center'     },
});
  


export default Favorites;
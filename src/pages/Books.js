import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  ScrollView,
} from "react-native";
import { BlocBook } from "../components/blocBook.js";
import { ListeBooks } from "../redux/Actions/booksAction";
import { useDispatch, useSelector } from "react-redux";
import { Input,Icon } from "@ui-kitten/components";
const App = (props) => {
  const [data, setFilteredDataSource] = useState();
  const renderItem = ({ item }) => (
    <BlocBook
      item={item}
      GetDetail={() => props.navigation.navigate("DetailBook", { id: item.id })}
    />
  );
  const dispatch = useDispatch();
  const Data = useSelector((state) => state.booksReducer.books);
 
  useEffect(() => {
    dispatch(ListeBooks());
  }, []);

  useEffect(() => {
    setFilteredDataSource(Data);
  }, [Data]);

  const renderIcon = (props) => (
  
      <Icon {...props} name={'search'} />
 
  );

  const searchFilterFunction = (text) => {
    // Check if searched text is not blank
    if (text) {
      // Inserted text is not blank
      // Filter the masterDataSource and update FilteredDataSource
      const newData = data.filter(function (item) {
        // Applying filter for the inserted text in search bar
        const itemData = item.title
          ? item.title.toUpperCase()
          : "".toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
    } else {
      // Inserted text is blank
      // Update FilteredDataSource with masterDataSource
      setFilteredDataSource(Data);
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      <Input
        style={{marginTop:10, width:"70%"}}
        placeholder="search ..."
        onChangeText={(text) => searchFilterFunction(text)}
        accessoryRight={renderIcon}
      />
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        numColumns={2}
      />
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#0A0A0A",
    alignItems: "center",
  },
});

export default App;

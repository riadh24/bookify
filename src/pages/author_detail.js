import React, { useEffect, useState } from "react";
import { View, StyleSheet, FlatList } from "react-native";
import { Button, Avatar, Text } from "@ui-kitten/components";
import { useFocusEffect } from "@react-navigation/native";
import { BlocBook } from "../components/blocBook";
import { useDispatch, useSelector } from "react-redux";
import { authordetail } from "../redux/Actions/authorDetailsAction";
 

export default AuthorDetails = (props) => {
  const author = useSelector((state) => state.authorReducer.author_details);
  const listBooks = useSelector((state) => state.authorReducer.listBooks);
  
 const dispatch= useDispatch()
  useFocusEffect(
    React.useCallback(() => {
      dispatch(authordetail(props.route.params.id));
    }, [])
  );

  const renderItem = ({ item }) => (
    <BlocBook
      item={item}
      GetDetail={() => props.navigation.navigate("DetailBook", { id: item.id })}
    />
  );

  return (
    <>
      {author && (
        <View style={{ flex: 1 }}>
          <View
            style={{
              flex: 2,
              backgroundColor: "#0A0A0A",
              justifyContent: "center",
            }}
          >
            <View style={{ alignItems: "center" }}>
              <Avatar
                style={styles.avatar}
                size="giant"
                source={{
                  uri: author.img,
                }}
              />
              <Text
                category="p1"
                style={{ fontWeight: "bold", color: "white" }}
              >
                {author.name}
              </Text>
            </View>

            <View
              style={{
                alignSelf: "center",
                width: "80%",
                justifyContent: "space-around",
                flexDirection: "row",
                marginTop: 20,
              }}
            >
              <View style={{ alignItems: "center" }}>
                <Text style={{ color: "white", fontSize: 20 }}>Books </Text>
                <Text style={{ color: "white" }}>20</Text>
              </View>
              <View
                style={{
                  alignItems: "center",
                }}
              >
                <Text style={{ color: "white", fontSize: 20 }}>Awards</Text>
                <Text style={{ color: "white" }}>{author.awards}</Text>
              </View>
              <View style={{ alignItems: "center" }}>
                <Text style={{ color: "white", fontSize: 20 }}>Followers</Text>
                <Text style={{ color: "white" }}>{author.followers}</Text>
              </View>
            </View>
          </View>

          <View style={{ flex: 2, backgroundColor: "#454545" }}>
            <Button
              status="primary"
              style={{
                borderColor: "white",
                backgroundColor: "white",
                alignSelf: "center",
                width: "50%",
                borderRadius: 50,
                marginTop: -20,
              }}
            >
              <Text style={{ fontWeight: "bold", fontSize: 15, color: "#000" }}>
                Follow
              </Text>
            </Button>
            <View style>
              <Text
                style={{
                  fontWeight: "bold",
                  margin: 15,
                  color: "white",
                  fontSize: 16,
                }}
              >
                List of books
              </Text>
            </View>

            {listBooks && (
              <FlatList
                data={listBooks}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
                numColumns={2}
              />
            )}
          </View>
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "wrap",
    padding: 8,
  },
  avatar: {
    width: 100,
    height: 100,
    margin: 8,
  },
});

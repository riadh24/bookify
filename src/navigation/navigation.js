import React from 'react';
import Books from '../pages/Books'
import Favorites from '../pages/Favorites'
import DetailBook from '../pages/detailBook'
import AuthorDetails from '../pages/author_detail'
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';
import LoginScreen from '../components/auth/loginScreen';
import InitScreen from '../components/initView';
import MyTabBar from '../components/MyTabBar';
const {Navigator, Screen} = createStackNavigator();

function AuthStack() {
  return (
    <NavigationContainer independent={true}>
    <Navigator>
    <Screen name="InitScreen" component={InitScreen} options= {{headerShown : false} } />
    <Screen name="MyTabs" component={MyTabs} options= {{headerShown : false} } />
    <Screen name="LoginScreen" component={LoginScreen} options= {{headerShown : false} }/> 
    </Navigator>
    </NavigationContainer>
  );
}
const Tab = createBottomTabNavigator();
function MyTabs() {
  return (
    <NavigationContainer independent={true}>
    <Tab.Navigator tabBar={props => <MyTabBar {...props} />} >
    <Tab.Screen name="Books" component={Books} options= {{headerShown : false} } />
    <Tab.Screen name="Favorites" component={Favorites} options= {{headerShown : false} } />
    <Tab.Screen name="DetailBook" component={DetailBook} options={{ 
          title: 'book Details',
          headerStyle: {
            backgroundColor: '#0A0A0A',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}/>
    <Tab.Screen name="AuthorDetails" component={AuthorDetails} options={{
          title: 'Author Details',
          headerStyle: {
            backgroundColor: '#0A0A0A',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          
        }} />
        </Tab.Navigator>
    </NavigationContainer>

  );
}

export default AuthStack;

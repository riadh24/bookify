 import axios from 'axios';
 import Common from './Common';
 import AsyncStorage from '@react-native-community/async-storage';

//export const httpClient=()=>{
    console.log('httpClient first');
    let nav=null;
    export const setNavigation=(navigation)=>{
       nav=navigation;
    }
    const loginUrl = "/login";
    axios.defaults.baseURL=Common.apiUrl;
    axios.interceptors.request.use(async (config)=>{
        const jwtToken= await AsyncStorage.getItem('@data');
        if(jwtToken!=null && config.url.search(loginUrl)=== -1){
            config.headers={'Accept': 'application/json', 'Authorization':"Bearer " + JSON.parse(jwtToken).token}
            console.log("config.headers",config.headers);
        }
        return config
    });
 
      export const  httpClient=axios


  
